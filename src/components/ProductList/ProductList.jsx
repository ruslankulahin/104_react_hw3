import PropTypes from "prop-types";
import ProductCard from "../ProductCard";
import "./ProductList.scss";

export default function ProductList(props) {
    const {
        products,
        cartItems,
        favoriteItems,
        AddToCart,
        AddToFavorites,
        showCartModal,
    } = props;
    return (
        <div className="container">
            <div className="product-list">
                {products.map((product) => (
                    <ProductCard
                        key={product.id}
                        product={product}
                        inCart={cartItems.some(
                            (item) => item.id === product.id
                        )}
                        inFavorites={favoriteItems.some(
                            (item) => item.id === product.id
                        )}
                        AddToCart={AddToCart}
                        AddToFavorites={AddToFavorites}
                        showCartModal={showCartModal}
                    />
                ))}
            </div>
        </div>
    );
}

ProductList.propTypes = {
    products: PropTypes.array,
    cartItems: PropTypes.array,
    favoriteItems: PropTypes.array,
    AddToCart: PropTypes.func,
    AddToFavorites: PropTypes.func,
    showCartModal: PropTypes.func,
    inCart: PropTypes.bool,
    inFavorites: PropTypes.bool,
};
