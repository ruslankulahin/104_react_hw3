import cn from "classnames";
import PropTypes from "prop-types";
import "./Modal.scss";

export default function Modal(props) {
    const {
        header,
        classNames,
        closeButton,
        totalPrice,
        text,
        actions,
        closeModal,
        handleOutsideClick,
    } = props;
    return (
        <div className="modal-wrapper" onClick={handleOutsideClick}>
            <div className={cn("modal", classNames)}>
                <div className="modal-content">
                    <div className="modal-header">
                        <h2>{header}</h2>
                        {closeButton && (
                            <span className="modal-close" onClick={closeModal}>
                                &times;
                            </span>
                        )}
                    </div>
                    <div className="body-text">{text}</div>
                    <div className="modal-body">{actions}</div>
                    <div className="modal-footer">{totalPrice}</div>
                </div>
            </div>
        </div>
    );
}

Modal.propTypes = {
    header: PropTypes.string,
    classNames: PropTypes.string,
    closeButton: PropTypes.any,
    totalPrice: PropTypes.object,
    text: PropTypes.any,
    actions: PropTypes.any,
    closeModal: PropTypes.func,
    handleOutsideClick: PropTypes.func,
};
