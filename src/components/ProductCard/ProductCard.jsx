import PropTypes from "prop-types";
import Button from "../Button";
import { faStar } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import "./ProductCard.scss";

export default function ProductCard(props) {
    const { name, price, image } = props.product;
    const { inCart, inFavorites } = props;

    const handleClickAddToCart = () => {
        props.AddToCart(props.product);
    };

    const handleClickAddToFavorites = () => {
        props.AddToFavorites(props.product);
    };

    const handleShowCartModal = () => {
        props.showCartModal(props.product);
    };

    return (
        <div className="product-card">
            <div className="image-wrapper">
                <img src={image} alt={name} />
            </div>
            <h3 className="product-name">{name}</h3>
            <div className="card-footer">
                {!inFavorites && (
                    <Button
                        className="add-favorite"
                        onClick={handleClickAddToFavorites}
                    >
                        <FontAwesomeIcon
                            className="icon-star"
                            icon={faStar}
                            size="xs"
                            style={{ 
                                color: "#9a9a9c",
                            }}
                        />
                    </Button>
                )}
                {inFavorites && (
                    <Button
                        className="add-favorite"
                        style={{
                            backgroundColor: "#dddddd",
                            borderRadius: "60px",
                        }}
                        onClick={handleClickAddToFavorites}
                    >
                        <FontAwesomeIcon
                            icon={faStar}
                            size="xs"
                            style={{ color: "#f4f88b" }}
                        />
                    </Button>
                )}
                <p className="product-price">{price} грн</p>
            </div>
            {!inCart && (
                <Button className="add-to-cart" onClick={handleClickAddToCart}>
                    Додати до кошика
                </Button>
            )}
            {inCart && (
                <Button
                    className="remove-to-cart"
                    onClick={handleShowCartModal}
                >
                    Товар у кошику
                </Button>
            )}
        </div>
    );
}

ProductCard.propTypes = {
    product: PropTypes.shape({
        name: PropTypes.string,
        price: PropTypes.number,
        image: PropTypes.string,
    }),
    children: PropTypes.any,
    inCart: PropTypes.bool,
    inFavorites: PropTypes.bool,
    AddToCart: PropTypes.func,
    AddToFavorites: PropTypes.func,
    showCartModal: PropTypes.func,
};
